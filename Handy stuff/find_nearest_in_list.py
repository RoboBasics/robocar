#! /usr/bin/python

import time
import argparse
import math

#--------------------------------------------------------------------------------------------------------- Set up a parser for command line arguments
parser = argparse.ArgumentParser( "Gets images from the robot without callback" )
parser.add_argument( "hostname", default="localhost", nargs='?', help="The ip address of the robot" )
args = parser.parse_args()
# -------------------------------------------------------------------------------------------------------- Connect to the robot
#bot = py_websockets_bot.WebsocketsBot( args.hostname )                               # When running a local script on the Pi
#bot = py_websockets_bot.WebsocketsBot( "192.168.42.1" )                               # When running a script at a remote computer using webSockets
# -------------------------------------------------------------------------------------------------------- Initializing variables
my_list = [-90,-75,-60,-45,-30,-15,0,15,30,45,60,75,90]
#---------------------------------------------------------------------------------------------------------
if __name__ == "__main__":
    my_item = -36

    while my_item <= 90:
        item_list = min(my_list, key=lambda x:abs(x-my_item))
        print my_item, '==>', item_list
        my_item += 7
    
