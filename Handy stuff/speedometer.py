#! /usr/bin/python

# To determine the max speed of the bot
# Runs local and with the webserver down:
#             - sudo service robot_web_server stop
#             - cd raspberry_py_camera_bot
#             - sudo python speedometer.py
# Functions:
#           robot_init ()
#           drive_straight(meters_to_go)
#           write_error_values()
# Writes average ticks and average m/s each interval 

import time
import cv2
import mini_driver
import pi_sensor_reader
import csv

#-----------------------------------------------------------------------------
def robot_init ():

    global Vmax_meter_per_second, Vmax_perc, \
           ticks_per_meter, ticks_per_degree, \
           Vmax_ticks_per_second, Vstall_ticks_per_second, \
           Vtarget_ticks_per_interval, encoder_prev_l, encoder_prev_r,\
           interval, min_interval, max_interval, counter, \
           Vperc_l, Vperc_r, miniDriver, \
           Vticks_avg_l, Vticks_avg_l, Vms_avg_l, Vms_avg_r, \
           Aticks_l, Aticks_r, Vms_avg_l, Vms_avg_r, Ams_l, Ams_r, \
           compass, heading

    miniDriver = mini_driver.MiniDriver()
    connected = miniDriver.connect()
    print 'connected to mini driver =', connected

    sensorConfiguration = mini_driver.SensorConfiguration(
        configD12=mini_driver.PIN_FUNC_ULTRASONIC_READ, 
        configD13=mini_driver.PIN_FUNC_DIGITAL_READ, 
        configA0=mini_driver.PIN_FUNC_ANALOG_READ, 
        configA1=mini_driver.PIN_FUNC_DIGITAL_READ,
        configA2=mini_driver.PIN_FUNC_DIGITAL_READ, 
        configA3=mini_driver.PIN_FUNC_DIGITAL_READ,
        configA4=mini_driver.PIN_FUNC_DIGITAL_READ, 
        configA5=mini_driver.PIN_FUNC_DIGITAL_READ,
        leftEncoderType=mini_driver.ENCODER_TYPE_SINGLE_OUTPUT, 
        rightEncoderType=mini_driver.ENCODER_TYPE_SINGLE_OUTPUT )
    miniDriver.setSensorConfiguration( sensorConfiguration )
    miniDriver.update()
    
    compass = pi_sensor_reader.PiSensorReader()
    #----------------------------------------------------------------------------- Init variables
    Vmax_perc = 100.0
    ticks_per_meter = 97.9415
    ticks_per_degree = 0.1256
    interval = 0.03
    min_interval = 0.026
    max_interval = 0.034
    heading = 0.0
    counter = 0.0
    Vticks_avg_l = 0.0
    Vticks_avg_l = 0.0
    Vms_avg_l = 0.0
    Vms_avg_r = 0.0
    Aticks_l = 0.0 
    Aticks_r = 0.0
    Vms_avg_l = 0.0
    Vms_avg_r = 0.0
    Ams_l = 0.0
    Ams_r = 0.0

    Vperc_l = 0.0
    Vperc_r = 0.0
    miniDriver.setOutputs( Vperc_l, Vperc_r, 90, 90 )
#-----------------------------------------------------------------------------
def time_out (milsec):
    for i in xrange (milsec):
        time.sleep (0.001)
#-----------------------------------------------------------------------------
def reset_encoder_ticks ():
    global encoder_l, encoder_r, error_b, \
           encoder_start_l, encoder_start_r, encoder_start_b
    
    encoder_start_l = encoder_l
    encoder_start_r = encoder_r
#-----------------------------------------------------------------------------
def get_actual_encoder_ticks():
    global encoder_l, encoder_r, error_b, \
           encoder_start_l, encoder_start_r, encoder_start_b
    
    encoder_l -= encoder_start_l
    encoder_r -= encoder_start_r
    
#-----------------------------------------------------------------------------
def get_sensor_readings ():
    global ir_range, encoder_l, encoder_r, error_b, us_range, miniDriver, heading

    miniDriver.update()
    ir_range = miniDriver.getDigitalReadings().data 
    encoder_data = miniDriver.getEncodersReading().data
    encoder_l = encoder_data [0]
    encoder_r = encoder_data [1]
    us_range = miniDriver.getUltrasonicReading().data
    heading = compass.read_pi_sensors()
    heading = round ((heading), 1)
    
#-----------------------------------------------------------------------------
def drive_straight (meters_to_go): 
    global heading, \
           interval, counter, Vperc_l, Vperc_r, Vticks_avg_l, Vms_avg_l, \
           Vticks_per_second_l, Vticks_per_second_r, Vticks_avg_r, Vms_avg_r,\
           Aticks_l, Aticks_r, Vms_avg_l, Vms_avg_r, Ams_l, Ams_r, \
           Vmax_perc, encoder_l, encoder_r, \
           encoder_prev_l, encoder_prev_r, \
           errorValuesList
    
    errorValuesList = []
    get_sensor_readings()
    reset_encoder_ticks()
    get_actual_encoder_ticks()
    Vperc_l = Vmax_perc
    Vperc_r = Vmax_perc
    miniDriver.setOutputs( Vperc_l, Vperc_r, 90, 90 )
    interval_start = time.clock()       
    meters_made = 0
    while meters_made <= meters_to_go:
        interval = round((time.clock () - interval_start), 2)
        if interval > min_interval:
            interval_start = time.clock()
            counter = counter + interval
            Vticks_avg_l = round((( encoder_l - encoder_prev_l)/interval),1)
            Vticks_avg_r = round((( encoder_r - encoder_prev_r)/interval),1)
            Vms_avg_l = round((Vticks_avg_l / ticks_per_meter),2)
            Vms_avg_r = round((Vticks_avg_r / ticks_per_meter),2)
            encoder_prev_l = encoder_l
            encoder_prev_r = encoder_r 
            keep_error_values()
        miniDriver.setOutputs( Vperc_l, Vperc_r, 90, 90 )
        get_sensor_readings ()
        get_actual_encoder_ticks()
        ticks_made = max (encoder_l, encoder_r)
        meters_made = ticks_made / ticks_per_meter
    miniDriver.setOutputs( 0.0, 0.0, 90, 90 )
#-----------------------------------------------------------------------------
def keep_error_values():
    global errorValuesList
    errorValues = {}
    errorValues[ "Clock"   ] = counter
    errorValues[ "Sample"  ] = interval
    errorValues[ "Encdr_l" ] = encoder_l
    errorValues[ "TpS_l"   ] = Vticks_avg_l
    errorValues[ "MpS_l"   ] = Vms_avg_l
    errorValues[ "Encdr_r" ] = encoder_r
    errorValues[ "TpS_r"   ] = Vticks_avg_r
    errorValues[ "MpS_r"   ] = Vms_avg_r
    errorValues[ "Compass" ] = heading
    errorValuesList.append( errorValues )
#-----------------------------------------------------------------------------
def write_error_values():
    global errorValuesList
    
    outputFilename = "Speedometer_values_{0}.csv".format( int( time.time() ) )
    with open( outputFilename, "w" ) as csvFile:
        dictWriter = csv.DictWriter( csvFile, 
            [ "Clock", "Sample", 
              "Encdr_l", "TpS_l", "MpS_l",
              "Encdr_r", "TpS_r", "MpS_r",
              "Compass"] )
        dictWriter.writeheader()
        dictWriter.writerows( errorValuesList )
#-----------------------------------------------------------------------------
if __name__ == "__main__":

    robot_init ()   
    drive_straight (2)
    write_error_values()
    miniDriver.disconnect ()
