import RPi.GPIO as GPIO
import smbus
import time
import math

rev = GPIO.RPI_REVISION
if rev == 2 or rev == 3:
    bus = smbus.SMBus(1)
else:
    bus = smbus.SMBus(0)


def read_byte(adr):
    return bus.read_byte_data(address, adr)

def read_word(adr):
    high = bus.read_byte_data(address, adr)
    low = bus.read_byte_data(address, adr+1)
    val = (high << 8) + low
    return val

def read_word_2c(adr):
    val = read_word(adr)
    if (val >= 0x8000):
        return -((65535 - val) + 1)
    else:
        return val

def write_byte(adr, value):
    bus.write_byte_data(address, adr, value)

address = 0x1e
declination = 0.42
scale = 0.92
x_offset = 25
y_offset = -4
    
x_out = 0.0
y_out = 0.0
z_out = 0.0

write_byte(0, 0b01110000) # Set to 8 samples @ 15Hz
write_byte(1, 0b00100000) # 1.3 gain LSb / Gauss 1090 (default)
write_byte(2, 0b00000000) # Continuous sampling

def read_compass():

    x_out = (read_word_2c(3) - x_offset) * self.scale
    y_out = (read_word_2c(7) - y_offset) * self.scale
    z_out = (read_word_2c(5))* scale

    bearing  = math.atan2(y_out, x_out) 
    if (bearing < 0):
        bearing += 2 * math.pi
    
    bearing = math.degrees(bearing) + declination
        
    return bearing
