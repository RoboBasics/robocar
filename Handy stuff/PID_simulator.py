#! /usr/bin/python

#-----------------------------------------------------------------------------------------------------------------------------------------------------------
# Simulation of a single pid controler based on the characteristics of RB2 
# Purpose is fast finding of initial settings for the PID gains
#
# Robot characteristics used in this script: 
# wheel_center_distance	  = 0.147  m
# wheel_diametre	  = 0.065  m
# wheel _perimeter	  = 0.2042 m
# max_speed               = 0.68   m/s
# ticks_per_revolution	  = 20
# ticks_per_metre	  = 97.943 (ticks_per_revolution / wheel _perimeter)
# max_speed_ticks_second  = 66.601 (speed_max * ticks_per_metre)
#
# Calculated characteristics:
# speed_metres_per_second = perc_speed / 100 * speed_max (i.e. percentage of the maximum speed)
# 
# The optical encoder can only generate 33 ticks/sec => sample frequency is set to 25 (0.04 sec) to generate at least 1 tick
#
# The script starts with an arbitrary measurement of 1;
# Each parse the pid outcome is added to the actuator value and to the percentual speed to simulate the bots behaviour
# Output values are being written to a csv file for easy import in excel
#-----------------------------------------------------------------------------------------------------------------------------------------------------------

import numpy as np
import csv
import time
  
# -------------------------------------------------------------------------------------    
Kp = 0.5                                                                              # proportional gain
Ki = 0.6                                                                              # integrating gain
Kd = 0.001                                                                            # differential gain 
setpoint = 1.332027424
sample_time = 0.04
max_speed = 0.68
perc_speed = 50
max_perc_speed = 100
ticks_per_metre = 97.94319295
errorValuesList = []
values_P = []
values_I =[]
values_D = []
error_sum = 0
error_prev = 0
actuator = 1
cnt = 0
missed = 0
# -------------------------------------------------------------------------------------
if __name__ == "__main__":
    script_start = time.time()
    sample_start = script_start
    while cnt < 200:
        current_time = time.time()
        timestamp = round ((current_time - script_start), 2)
        sample_time = round((current_time - sample_start), 2)
        if sample_time >= 0.04:
            sample_start = time.time()
            if sample_time <= 0.06:                                                   # to keep intervals equal (!!) 
                error = setpoint - actuator
                error_sum = error_sum + error 
                P = Kp * error
                I = Ki * error_sum * sample_time
                D = Kd * (error - error_prev) / sample_time
                accumulated =  P + I + D      
                errorValues = {}
                errorValues[ "timestamp" ] = timestamp
                errorValues[ "sample_time" ] = sample_time
                errorValues[ "speed" ] = perc_speed
                errorValues[ "actuator" ] = actuator
                errorValues[ "setpoint" ] = setpoint
                errorValues[ "error" ] = error
                errorValues[ "error_sum" ] = error_sum
                errorValues[ "proportional" ] = P
                errorValues[ "integrated" ] = I
                errorValues[ "differential" ] = D
                errorValues[ "accumulated" ] = accumulated
                errorValuesList.append( errorValues )
                error_prev = error
                actuator = actuator + accumulated
                perc_speed = perc_speed + accumulated
                setpoint = perc_speed / max_perc_speed * max_speed * ticks_per_metre * sample_time
                cnt += 1
            else:
                missed += 1 
    # ---------------------------------------------------------------------------------
    print 'samples dropped =', missed
    outputFilename = "values_{0}.csv".format( int( time.time() ) )
    with open( outputFilename, "w" ) as csvFile:
        dictWriter = csv.DictWriter( csvFile, 
            [ "timestamp", "sample_time", "speed", "actuator", "setpoint", "error",
              "error_sum", "proportional", "integrated", "differential", "accumulated"] )
        dictWriter.writeheader()
        dictWriter.writerows( errorValuesList )
    
