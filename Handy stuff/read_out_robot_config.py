#! /usr/bin/python
#----------------------------------------------------------------------------------------------------------------------------------------- 
# Content of py_websockets_bot.WebsocketsBot.robot_config() :
# panPulseWidthMin: 500
# panPulseWidthMax: 1900
# tiltPulseWidthMin: 544
# tiltPulseWidthMax: 2400
# usePresetMotorSpeeds: True
# customMaxAbsMotorSpeed: 80.0
# customMaxAbsTurnSpeed: 50.0
# leftMotorScale: 1.02
#    miniDriverSensorConfiguration: configD12: ultrasonic
#    configD13: inactive
#    configA0: analog
#    configA1: digital
#    configA2: digital
#    configA3: digital
#    configA4: digital
#    configA5: digital
#    leftEncoderType: single_output
#    rightEncoderType: single_output
#    piSensorModuleName: sensors.default_sensor_reader
#
# Content of status_dic:
#
# {u'batteryVoltage': 5.171065493646139,
#  u'sensors': {u'batteryVoltage': {u'timestamp': , u'data': },
#               u'encoders':       {u'timestamp': , u'data': },
#               u'ultrasonic':     {u'timestamp': , u'data': },
#               u'analog':         {u'timestamp': , u'data': },
#               u'digital':        {u'timestamp': , u'data': }},
#  u'presetMaxAbsMotorSpeed': 80.0,
#  u'presetMaxAbsTurnSpeed': 50.0}
#----------------------------------------------------------------------------------------------------------------------------------------- 
import time
import argparse
import cv2
import numpy as np
import py_websockets_bot
import py_websockets_bot.mini_driver
import py_websockets_bot.robot_config
import random
import csv
import pid
#import RPi.GPIO as GPIO

#--------------------------------------------------------------------------------------------------------- Set up a parser for command line arguments
parser = argparse.ArgumentParser( "Gets images from the robot without callback" )
parser.add_argument( "hostname", default="localhost", nargs='?', help="The ip address of the robot" )
args = parser.parse_args()
# -------------------------------------------------------------------------------------------------------- Connect to the robot
#bot = py_websockets_bot.WebsocketsBot( args.hostname )                               # When running a local script on the Pi
bot = py_websockets_bot.WebsocketsBot( "192.168.42.1" )                               # When running a script at a remote computer using webSockets
# -------------------------------------------------------------------------------------------------------- Configure the sensors on the robot
sensorConfiguration = py_websockets_bot.mini_driver.SensorConfiguration(
    configD12=py_websockets_bot.mini_driver.PIN_FUNC_ULTRASONIC_READ, 
    configD13=py_websockets_bot.mini_driver.PIN_FUNC_INACTIVE, 
    configA0=py_websockets_bot.mini_driver.PIN_FUNC_ANALOG_READ, 
    configA1=py_websockets_bot.mini_driver.PIN_FUNC_DIGITAL_READ,
    configA2=py_websockets_bot.mini_driver.PIN_FUNC_DIGITAL_READ, 
    configA3=py_websockets_bot.mini_driver.PIN_FUNC_DIGITAL_READ,
    configA4=py_websockets_bot.mini_driver.PIN_FUNC_DIGITAL_READ, 
    configA5=py_websockets_bot.mini_driver.PIN_FUNC_DIGITAL_READ,
    leftEncoderType=py_websockets_bot.mini_driver.ENCODER_TYPE_SINGLE_OUTPUT, 
    rightEncoderType=py_websockets_bot.mini_driver.ENCODER_TYPE_SINGLE_OUTPUT )
robot_config = bot.get_robot_config()
robot_config.miniDriverSensorConfiguration = sensorConfiguration
bot.set_robot_config( robot_config )    
# -------------------------------------------------------------------------------------------------------- Update any background communications with the robot
bot.update()
time.sleep( 0.1 )                                                                                        # Sleep to avoid overload of the web server on the robot
# -------------------------------------------------------------------------------------------------------- Initializing variables
#---------------------------------------------------------------------------------------------------------
if __name__ == "__main__":
    bot.centre_neck()
    bot.update()
    status_dict, _ = bot.get_robot_status_dict()
    sensor_dict = status_dict[ "sensors" ]
    voltage_s = sensor_dict[ "batteryVoltage" ][ "data" ]
    encoders = sensor_dict[ "encoders" ][ "data" ]
    ultrasonic = sensor_dict[ "ultrasonic" ][ "data" ]
    analog = sensor_dict[ "analog" ][ "data" ]
    digital = sensor_dict[ "digital" ][ "data" ]
    PreSpeed = status_dict[ "presetMaxAbsMotorSpeed" ]
    PreTurn = status_dict[ "presetMaxAbsTurnSpeed" ]
    # -----------------------------------------------------
    voltage, _ = bot.get_battery_voltage()
    voltage = round(voltage,1)
    print 'Status_dict'
    print '    Voltage           ', voltage
    print '    Sensor_dict'
    print '                       Voltage   ', voltage_s
    print '                       Encoders  ', encoders
    print '                       Ultrasonic', ultrasonic
    print '                       Analog    ', analog
    print '                       Digital   ', digital
    print '    Preset motor speed', PreSpeed
    print '    Preset turn speed ', PreTurn
    encoders = sensor_dict[ "encoders" ][ "data" ]
    ultrasonic = sensor_dict[ "ultrasonic" ][ "data" ]
    analog = sensor_dict[ "analog" ][ "data" ]
    digital = sensor_dict[ "digital" ][ "data" ]
    print ''
    print sensor_dict
    print ''
    print status_dict
    print ''
    print robot_config
    print ''
#    content = dir(py_websockets_bot)
#    print content
#    print ''
#    content = dir(py_websockets_bot.mini_driver)
#    print content
#    print ''
#    content = dir(py_websockets_bot.robot_config)
#    print content
#    print ''
#    content = dir(py_websockets_bot.WebsocketsBot)
#    print content
#    print ''
#    content = dir(argparse)
#    print content
#    print ''
    content = dir(pid)
    print content
    print ''
