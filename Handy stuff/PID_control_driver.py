#! /usr/bin/python

# For running local, without the use of py_websockets_bot
# Run from raspberry_py_camera_bot.
# Functions:
#           robot_init ()
#           drive_straight(meters_to_go)
#           turn_degrees(direction, degrees)
#           write_error_values()
# At max speed of 0.31 m/s the minimum interval = 0.04 seconds
# to enable the optical encoders to generate at least 1 tick
# Further calibration can be applied by 'running squares'

import time
import cv2
import mini_driver
import pi_sensor_reader
import csv

#-----------------------------------------------------------------------------
def robot_init (P_gain_l, I_gain_l, D_gain_l, \
                P_gain_r, I_gain_r, D_gain_r, \
                P_gain_b, I_gain_b, D_gain_b ):

    global Vmax_meter_per_second, Vstall_perc, \
           ticks_per_meter, ticks_per_degree, \
           Vmax_ticks_per_second, Vstall_ticks_per_second, \
           Vtarget_ticks_per_interval, \
           Kp_l, Ki_l, Kd_l, Kp_r, Ki_r, Kd_r, Kp_b, Ki_b, Kd_b, \
           interval, min_interval, max_interval, \
           Vperc_l, Vperc_r, miniDriver, \
           error_sum_l, error_sum_r, error_sum_b, \
           error_prev_l, error_prev_r, error_prev_b, \
           compass, heading

    miniDriver = mini_driver.MiniDriver()
    connected = miniDriver.connect()
    print 'connected to mini driver =', connected
    sensorConfiguration = mini_driver.SensorConfiguration(
        configD12=mini_driver.PIN_FUNC_ULTRASONIC_READ, 
        configD13=mini_driver.PIN_FUNC_DIGITAL_READ, 
        configA0=mini_driver.PIN_FUNC_ANALOG_READ, 
        configA1=mini_driver.PIN_FUNC_DIGITAL_READ,
        configA2=mini_driver.PIN_FUNC_DIGITAL_READ, 
        configA3=mini_driver.PIN_FUNC_DIGITAL_READ,
        configA4=mini_driver.PIN_FUNC_DIGITAL_READ, 
        configA5=mini_driver.PIN_FUNC_DIGITAL_READ,
        leftEncoderType=mini_driver.ENCODER_TYPE_SINGLE_OUTPUT, 
        rightEncoderType=mini_driver.ENCODER_TYPE_SINGLE_OUTPUT )
    miniDriver.setSensorConfiguration( sensorConfiguration )
    miniDriver.update()
    
    compass = pi_sensor_reader.PiSensorReader()
    #----------------------------------------------------------------------------- Init variables
    Vmax_meter_per_second = 0.68
    Vstall_perc = 50.0
    ticks_per_meter = 97.9415
    ticks_per_degree = 0.1256
    Kp_l = P_gain_l
    Ki_l = I_gain_l
    Kd_l = D_gain_l
    Kp_r = P_gain_r
    Ki_r = I_gain_r
    Kd_r = D_gain_r
    Kp_b = P_gain_b
    Ki_b = I_gain_b
    Kd_b = D_gain_b
    interval = 0.03
    min_interval = 0.026
    max_interval = 0.034
    error_sum_l = 0.0
    error_sum_r = 0.0
    error_sum_b = 0.0
    error_prev_l = 0.0
    error_prev_r = 0.0
    error_prev_b = 0.0
    heading = 0.0
    Vmax_ticks_per_second = round((Vmax_meter_per_second * ticks_per_meter),2)    
    Vstall_ticks_per_second = round((Vstall_perc / 100 * Vmax_ticks_per_second),2)
    Vtarget_ticks_per_interval = round((Vmax_ticks_per_second * interval),2)
    Vperc_l = 0.0
    Vperc_r = 0.0
    miniDriver.setOutputs( Vperc_l, Vperc_r, 90, 90 )
#-----------------------------------------------------------------------------
def time_out (milsec):
    for i in xrange (milsec):
        time.sleep (0.001)
#-----------------------------------------------------------------------------
def reset_encoder_ticks ():
    global encoder_l, encoder_r, error_b, \
           encoder_start_l, encoder_start_r, encoder_start_b
    
    encoder_start_l = encoder_l
    encoder_start_r = encoder_r
#-----------------------------------------------------------------------------
def get_actual_encoder_ticks():
    global encoder_l, encoder_r, error_b, \
           encoder_start_l, encoder_start_r, encoder_start_b
    
    encoder_l -= encoder_start_l
    encoder_r -= encoder_start_r
    
#-----------------------------------------------------------------------------
def get_sensor_readings ():
    global ir_range, encoder_l, encoder_r, error_b, us_range, miniDriver, heading

    miniDriver.update()
    ir_range = miniDriver.getDigitalReadings().data 
    encoder_data = miniDriver.getEncodersReading().data
    encoder_l = encoder_data [0]
    encoder_r = encoder_data [1]
    us_range = miniDriver.getUltrasonicReading().data
    heading = compass.read_pi_sensors()
    heading = round ((heading), 0)
    
#-----------------------------------------------------------------------------
def update_setpoints():
    global encoder_l, encoder_r, Vtarget_ticks_per_interval, \
           setpoint_l, setpoint_r
    
    setpoint_l = round((encoder_l + Vtarget_ticks_per_interval),0)
    setpoint_r = round((encoder_r + Vtarget_ticks_per_interval),0)
#-----------------------------------------------------------------------------
def control_loop_motors ():

    global error_l, error_r, error_sum_l, error_sum_r, \
           Kp_l, Kp_r, Ki_l, Ki_r, Kd_l, Kd_r, \
           PID_l, PID_r, error_prev_l, error_prev_r, \
           Vticks_per_second_l, Vticks_per_second_r, \
           Vperc_l, Vperc_r, Vstall_perc, interval

    error_l = setpoint_l - encoder_l
    error_sum_l = error_sum_l + error_l 
    P = Kp_l * error_l
    I = Ki_l * error_sum_l * interval
    D = Kd_l * (error_l - error_prev_l) / interval
    PID_l =  P + I + D
    error_prev_l = error_l
    Vticks_per_second_l = Vticks_per_second_l + PID_l

    error_r = setpoint_r - encoder_r
    error_sum_r = error_sum_r + error_r 
    P = Kp_r * error_r
    I = Ki_r * error_sum_r * interval
    D = Kd_r * (error_r - error_prev_r) / interval
    PID_r =  P + I + D
    error_prev_r = error_r
    Vticks_per_second_r = Vticks_per_second_r + PID_r
#-----------------------------------------------------------------------------
def control_loop_balancing ():
    global error_b, setpoint_b, error_sum_b, interval, error_prev_b, \
           Kp_b, Kp_b, Ki_b, PID_b, \
           ticks_per_degree, Vticks_per_second_l, Vticks_per_second_r

    error_b = setpoint_b - heading
    error_sum_b = error_sum_b + error_b
    P = Kp_b * error_b
    I = Ki_b * error_sum_b * interval
    D = Kd_b * (error_b - error_prev_b) / interval
    PID_b =  (P + I + D) * ticks_per_degree
    error_prev_b = error_b
    Vticks_per_second_l = round((Vticks_per_second_l + 0.5 * PID_b), 0)
    Vticks_per_second_r = round((Vticks_per_second_r - 0.5 * PID_b), 0)
    Vticks_per_second_l = max ( Vstall_ticks_per_second,
                                min ( Vticks_per_second_l, Vmax_ticks_per_second))
    Vticks_per_second_r = max ( Vstall_ticks_per_second,
                                min ( Vticks_per_second_r, Vmax_ticks_per_second))
#-----------------------------------------------------------------------------
def drive_straight (meters_to_go): 
    global setpoint_b, heading, \
           interval, Vperc_l, Vperc_r, \
           Vticks_per_second_l, Vticks_per_second_r, \
           Vmax_ticks_per_second, Vstall_perc, encoder_l, encoder_r, \
           errorValuesList
    
    errorValuesList = []
    get_sensor_readings()
    reset_encoder_ticks()
    get_actual_encoder_ticks()
    update_setpoints ()
    setpoint_b = heading
    Vticks_per_second_l = Vstall_ticks_per_second
    Vticks_per_second_r = Vstall_ticks_per_second
    Vperc_l = Vstall_perc
    Vperc_r = Vstall_perc
    miniDriver.setOutputs( Vperc_l, Vperc_r, 90, 90 )
    interval_start = time.clock()       
    meters_made = 0
    while meters_made <= meters_to_go:
        interval = round((time.clock () - interval_start), 2)
        if interval > min_interval:
            interval_start = time.clock()                                    # Reset interval timer
            control_loop_motors()
            control_loop_balancing()
            Vperc_l = round((Vticks_per_second_l / Vmax_ticks_per_second * 100),1)
            Vperc_l = max ( Vstall_perc, min ( Vperc_l, 100 ) )
            Vperc_r = round((Vticks_per_second_r / Vmax_ticks_per_second * 100),1)
            Vperc_r = max ( Vstall_perc, min ( Vperc_r, 100 ) )
            keep_error_values()
            update_setpoints ()
        miniDriver.setOutputs( Vperc_l, Vperc_r, 90, 90 )
        get_sensor_readings ()
        get_actual_encoder_ticks()
        ticks_made = max (encoder_l, encoder_r)
        meters_made = ticks_made / ticks_per_meter
    miniDriver.setOutputs( 0.0, 0.0, 90, 90 )
    time_out(100)
#-----------------------------------------------------------------------------
def turn_degrees (direction, degrees):
    global encoder_l, encoder_r, ticks_per_degree
    
    if direction == 'left':
        Vperc_l = -84.0
        Vperc_r = 84.0
    else:
        Vperc_l = 100.0
        Vperc_r = -100.0
    get_sensor_readings()
    reset_encoder_ticks()
    get_actual_encoder_ticks()
    ticks_turned = max (encoder_l, encoder_r)
    target_ticks = round((degrees * ticks_per_degree),0)
    while ticks_turned < target_ticks:
        miniDriver.setOutputs( Vperc_l, Vperc_r, 90, 90 )
        get_sensor_readings()
        get_actual_encoder_ticks()
        ticks_turned = max (encoder_l, encoder_r)
        print 'ticks turned :', ticks_turned
    miniDriver.setOutputs( 0.0, 0.0, 90, 90 )
    time_out(100)
#-----------------------------------------------------------------------------
def keep_error_values():
    global errorValuesList
    errorValues = {}
    errorValues[ "Tsample" ] = interval
    errorValues[ "Vperc_l" ] = Vperc_l
    errorValues[ "Vticks_l"] = Vticks_per_second_l
    errorValues[ "setpnt_l" ] = setpoint_l
    errorValues[ "encdr_l" ] = encoder_l
    errorValues[ "error_l" ] = error_l
    errorValues[ "PID_l" ] = PID_l
    errorValues[ "Vperc_r" ] = Vperc_r
    errorValues[ "Vticks_r"] = Vticks_per_second_r
    errorValues[ "setpnt_r" ] = setpoint_r
    errorValues[ "encdr_r" ] = encoder_r
    errorValues[ "error_r" ] = error_r
    errorValues[ "PID_r" ] = PID_r
    errorValues[ "Compass" ] = heading
    errorValues[ "error_b" ] = error_b
    errorValues[ "PID_b" ] = PID_b
    errorValuesList.append( errorValues )
#-----------------------------------------------------------------------------
def write_error_values():
    global errorValuesList
    
    outputFilename = "Odm_1_values_{0}.csv".format( int( time.time() ) )
    with open( outputFilename, "w" ) as csvFile:
        dictWriter = csv.DictWriter( csvFile, 
            [ "Tsample",
              "Vperc_l", "Vticks_l", "setpnt_l", "encdr_l", "error_l", "PID_l", 
              "Vperc_r", "Vticks_r", "setpnt_r", "encdr_r", "error_r", "PID_r",
              "Compass","error_b", "PID_b"] )
        dictWriter.writeheader()
        dictWriter.writerows( errorValuesList )
#-----------------------------------------------------------------------------
def stop_robot():
    miniDriver.disconnect ()
#-----------------------------------------------------------------------------