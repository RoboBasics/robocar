#! /usr/bin/python

#-----------------------------------------------------------------------------------------------------------------------------------------------------------

#-----------------------------------------------------------------------------------------------------------------------------------------------------------
import time
import argparse
import cv2
import numpy as np
import math 
import py_websockets_bot
import py_websockets_bot.mini_driver
import py_websockets_bot.robot_config
import random
import csv
# -------------------------------------------------------------------------------------
# Set and initialize variables
# -------------------------------------------------------------------------------------
range_save = 40                                                                       # Safety limit 
motor_speed = 50.0                                                                    # = 50%; corr. to PMW 50Hz
encoder_data_start_l = 0.0
encoder_data_start_r = 0.0
pan_angle = 90
tilt_angle = 90
motor_l = 50.0
motor_r = 50.0
list_encoder = []
status_list = ['Running straight','Backing_out','Finding free range','Turn right','Turn left']
bot_status = status_list [1]
line_cnt = 25
font = cv2.FONT_HERSHEY_SIMPLEX
latest_camera_image = None
# -------------------------------------------------------------------------------------
def display():
    image, _  = bot.get_latest_camera_image()
    encoders = 'encoders   %d : %d ' % (encoder_left, encoder_right)
    distance = 'range      %d ' % (usrange_data)
    infrared = 'IR sensors %d ' % (infrared_data)
    speeds = 'motors     %d : %d ' % (motor_l, motor_r)
    cv2.putText(image, infrared ,(20, 25), font, 0.5,(0,255,0),2)
    cv2.putText(image, distance,(20, 45), font, 0.5,(0,255,0),2)    
    cv2.putText(image, encoders,(20, 65), font, 0.5,(0,0,255),2)
    cv2.putText(image, speeds,(20, 85), font, 0.5,(0,0,255),2)
    cv2.imshow("running",image)
    cv2.waitKey(1)
#-------------------------------------------------------------------------------------- Timer routine for accuracy
def time_out (milsec):
    for i in xrange (milsec):
        time.sleep (0.001)
# -------------------------------------------------------------------------------------  
def encoder_init ():
    global encoder_data_start_l, encoder_data_start_r
    encoder_data_start_l = encoder_data [0]
    encoder_data_start_r = encoder_data [1]
    print 'encoders initialized'
# -------------------------------------------------------------------------:------------  
def read_sensors ():
    global infrared_data, encoder_data, encoder_left, encoder_right, encoder_offset, usrange_data 
    status_dict, _ = bot.get_robot_status_dict()
    sensor_dict = status_dict[ "sensors" ]
    infrared_data = sensor_dict[ "digital" ][ "data" ]
    encoder_data = sensor_dict[ "encoders" ][ "data" ]
    encoder_left = encoder_data [0] - encoder_data_start_l
    encoder_right = encoder_data [1]- encoder_data_start_r
    encoder_offset = encoder_left - encoder_right
    usrange_data = sensor_dict[ "ultrasonic" ][ "data" ]
#--------------------------------------------------------------------------------------- Set up a parser for command line arguments
parser = argparse.ArgumentParser( "Gets images from the robot using callbacks and displays them" )
parser.add_argument( "hostname", default="localhost", nargs='?',
                     help="The ip address of the robot" )
args = parser.parse_args()
# -------------------------------------------------------------------------------------- Connect to the robot
bot = py_websockets_bot.WebsocketsBot( "192.168.42.1" )
#--------------------------------------------------------------------------------------- Initialize mini driver pins       
sensorConfiguration = py_websockets_bot.mini_driver.SensorConfiguration(
    configD12=py_websockets_bot.mini_driver.PIN_FUNC_ULTRASONIC_READ,                  # SeeeD 3-pin Ultrasonic sensor
    configD13=py_websockets_bot.mini_driver.PIN_FUNC_INACTIVE, 
    configA0=py_websockets_bot.mini_driver.PIN_FUNC_ANALOG_READ, 
    configA1=py_websockets_bot.mini_driver.PIN_FUNC_DIGITAL_READ,                      # Sharp IR switch RIGHT
    configA2=py_websockets_bot.mini_driver.PIN_FUNC_DIGITAL_READ,                      # Sharp IR switch LEFT
    configA3=py_websockets_bot.mini_driver.PIN_FUNC_DIGITAL_READ,                      # Grove Line sensor RIGHT
    configA4=py_websockets_bot.mini_driver.PIN_FUNC_DIGITAL_READ,                      # Grove Line sensor MIDDLE
    configA5=py_websockets_bot.mini_driver.PIN_FUNC_DIGITAL_READ,                      # Grove Line sensor LEFT
    leftEncoderType=py_websockets_bot.mini_driver.ENCODER_TYPE_SINGLE_OUTPUT,          # Single encoder
    rightEncoderType=py_websockets_bot.mini_driver.ENCODER_TYPE_SINGLE_OUTPUT )        # Single encoder
robot_config = bot.get_robot_config()
robot_config.miniDriverSensorConfiguration = sensorConfiguration
bot.set_robot_config( robot_config )
bot.update()                                                                           # Update any background communications with the robot
time_out (100)                                                                         # Sleep to avoid overload of the web server on the robot
#--------------------------------------------------------------------------------------
if __name__ == "__main__":

    bot.centre_neck()
    read_sensors()
    encoder_init()
    while encoder_offset != 0:
        read_sensors ()
        time_out (1)
    #---------------------------------------------------------------------------------- camera off, no display, no update, no print, no append 
    time_start = time.time()
    time_end = time.time()
    lapse_time = round((time_end - time_start), 2)
    t = 1
    while lapse_time <= 30:
        #bot.update()
        #display()
        read_sensors()
        #print encoder_data, encoder_offset, usrange_data, infrared_data,
        #list_encoder.append ([encoder_data, encoder_offset, usrange_data, infrared_data])
        t += 1
        time_end = time.time()
        lapse_time = round((time_end - time_start), 2)
    time_end = time.time()
    lapse_time = round((time_end - time_start), 2)
    print ''
    print 'camera off, no display, no update, no print, no append'
    print t, 'times in ', lapse_time, 'seconds, =', round((t / lapse_time),1), ' times per second'
    #---------------------------------------------------------------------------------- camera off, no display, no update, PRINT, no append 
    time_start = time.time()
    time_end = time.time()
    lapse_time = round((time_end - time_start), 2)
    t = 1
    while lapse_time <= 30:
        #bot.update()
        #display()
        read_sensors()
        print encoder_data, encoder_offset, usrange_data, infrared_data,
        #list_encoder.append ([encoder_data, encoder_offset, usrange_data, infrared_data])
        t += 1
        time_end = time.time()
        lapse_time = round((time_end - time_start), 2)
    time_end = time.time()
    lapse_time = round((time_end - time_start), 2)
    print ''
    print 'camera off, no display, no update, PRINT, no append'
    print t, 'times in ', lapse_time, 'seconds, =', round((t / lapse_time),1), ' times per second'
    #---------------------------------------------------------------------------------- camera off, no display, no update, no print, APPEND 
    time_start = time.time()
    time_end = time.time()
    lapse_time = round((time_end - time_start), 2)
    t = 1
    while lapse_time <= 30:
        #bot.update()
        #display()
        read_sensors()
        #print encoder_data, encoder_offset, usrange_data, infrared_data,
        list_encoder.append ([encoder_data, encoder_offset, usrange_data, infrared_data])
        t += 1
        time_end = time.time()
        lapse_time = round((time_end - time_start), 2)
    time_end = time.time()
    lapse_time = round((time_end - time_start), 2)
    print ''
    print 'camera off, no display, no update, no print, APPEND'
    print t, 'times in ', lapse_time, 'seconds, =', round((t / lapse_time),1), ' times per second'
    #---------------------------------------------------------------------------------- CAMERA ON, no display, UPDATE, no print, no append 
    bot.start_streaming_camera_images()
    time_out(200)
    bot.update()
    time_start = time.time()
    time_end = time.time()
    lapse_time = round((time_end - time_start), 2)
    t = 1
    while lapse_time <= 30:
        bot.update()
        #display()
        read_sensors()
        #print encoder_data, encoder_offset, usrange_data, infrared_data,
        #list_encoder.append ([encoder_data, encoder_offset, usrange_data, infrared_data])
        t += 1
        time_end = time.time()
        lapse_time = round((time_end - time_start), 2)
    time_end = time.time()
    lapse_time = round((time_end - time_start), 2)
    print ''
    print 'CAMERA ON, no display, UPDATE, no print, no append'
    print t, 'times in ', lapse_time, 'seconds, =', round((t / lapse_time),1), ' times per second'
    #---------------------------------------------------------------------------------- CAMERA ON, no display, UPDATE, PRINT, no append 
    bot.update()
    time_start = time.time()
    time_end = time.time()
    lapse_time = round((time_end - time_start), 2)
    t = 1
    while lapse_time <= 30:
        bot.update()
        #display()
        read_sensors()
        print encoder_data, encoder_offset, usrange_data, infrared_data,
        #list_encoder.append ([encoder_data, encoder_offset, usrange_data, infrared_data])
        t += 1
        time_end = time.time()
        lapse_time = round((time_end - time_start), 2)
    time_end = time.time()
    lapse_time = round((time_end - time_start), 2)
    print ''
    print 'CAMERA ON, no display, UPDATE, PRINT, no append'
    print t, 'times in ', lapse_time, 'seconds, =', round((t / lapse_time),1), ' times per second'
    #---------------------------------------------------------------------------------- CAMERA ON, no display, UPDATE, no print, APPEND 
    bot.update()
    time_start = time.time()
    time_end = time.time()
    lapse_time = round((time_end - time_start), 2)
    t = 1
    while lapse_time <= 30:
        bot.update()
        #display()
        read_sensors()
        #print encoder_data, encoder_offset, usrange_data, infrared_data,
        list_encoder.append ([encoder_data, encoder_offset, usrange_data, infrared_data])
        t += 1
        time_end = time.time()
        lapse_time = round((time_end - time_start), 2)
    time_end = time.time()
    lapse_time = round((time_end - time_start), 2)
    print ''
    print 'CAMERA ON, no display, UPDATE, no print, APPEND'
    print t, 'times in ', lapse_time, 'seconds, =', round((t / lapse_time),1), ' times per second'
    #---------------------------------------------------------------------------------- CAMERA ON, DISPLAY, UPDATE, no print, no append 
    bot.update()
    time_start = time.time()
    time_end = time.time()
    lapse_time = round((time_end - time_start), 2)
    t = 1
    while lapse_time <= 30:
        bot.update()
        display()
        read_sensors()
        #print encoder_data, encoder_offset, usrange_data, infrared_data,
        #list_encoder.append ([encoder_data, encoder_offset, usrange_data, infrared_data])
        t += 1
        time_end = time.time()
        lapse_time = round((time_end - time_start), 2)
    time_end = time.time()
    lapse_time = round((time_end - time_start), 2)
    print ''
    print 'CAMERA ON, DISPLAY, UPDATE, no print, no append'
    print t, 'times in ', lapse_time, 'seconds, =', round((t / lapse_time),1), ' times per second'
    #---------------------------------------------------------------------------------- CAMERA ON, DISPLAY, UPDATE, PRINT, no append 
    bot.update()
    time_start = time.time()
    time_end = time.time()
    lapse_time = round((time_end - time_start), 2)
    t = 1
    while lapse_time <= 30:
        bot.update()
        display()
        read_sensors()
        print encoder_data, encoder_offset, usrange_data, infrared_data,
        #list_encoder.append ([encoder_data, encoder_offset, usrange_data, infrared_data])
        t += 1
        time_end = time.time()
        lapse_time = round((time_end - time_start), 2)
    time_end = time.time()
    lapse_time = round((time_end - time_start), 2)
    print ''
    print 'CAMERA ON, DISPLAY, UPDATE, PRINT, no append'
    print t, 'times in ', lapse_time, 'seconds, =', round((t / lapse_time),1), ' times per second'
    #---------------------------------------------------------------------------------- CAMERA ON, DISPLAY, UPDATE, no print, APPEND 
    bot.update()
    time_start = time.time()
    time_end = time.time()
    lapse_time = round((time_end - time_start), 2)
    t = 1
    while lapse_time <= 30:
        bot.update()
        display()
        read_sensors()
        #print encoder_data, encoder_offset, usrange_data, infrared_data,
        list_encoder.append ([encoder_data, encoder_offset, usrange_data, infrared_data])
        t += 1
        time_end = time.time()
        lapse_time = round((time_end - time_start), 2)
    time_end = time.time()
    lapse_time = round((time_end - time_start), 2)
    print ''
    print 'CAMERA ON, DISPLAY, UPDATE, no print, APPEND'
    print t, 'times in ', lapse_time, 'seconds, =', round((t / lapse_time),1), ' times per second'
    #---------------------------------------------------------------------------------- CAMERA ON, DISPLAY, UPDATE, PRINT, APPEND 
    bot.update()
    time_start = time.time()
    time_end = time.time()
    lapse_time = round((time_end - time_start), 2)
    t = 1
    while lapse_time <= 30:
        bot.update()
        display()
        read_sensors()
        print encoder_data, encoder_offset, usrange_data, infrared_data,
        list_encoder.append ([encoder_data, encoder_offset, usrange_data, infrared_data])
        t += 1
        time_end = time.time()
        lapse_time = round((time_end - time_start), 2)
    time_end = time.time()
    lapse_time = round((time_end - time_start), 2)
    print ''
    print 'CAMERA ON, DISPLAY, UPDATE, PRINT, APPEND'
    print t, 'times in ', lapse_time, 'seconds, =', round((t / lapse_time),1), ' times per second'
    # -------------------------------------------------------------------------------------
    cv2.destroyAllWindows()
    bot.stop_streaming_camera_images()
    bot.set_motor_speeds( 0.0, 0.0 )
    bot.centre_neck()
    bot.disconnect()
    print 'FINISHED'
