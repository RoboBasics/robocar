#! /usr/bin/env python

# Copyright (c) 2014, Dawn Robotics Ltd
# All rights reserved.

# Redistribution and use in source and binary forms, with or without 
# modification, are permitted provided that the following conditions are met:

# 1. Redistributions of source code must retain the above copyright notice, this
# list of conditions and the following disclaimer.

# 2. Redistributions in binary form must reproduce the above copyright notice, 
# this list of conditions and the following disclaimer in the documentation 
# and/or other materials provided with the distribution.

# 3. Neither the name of the Dawn Robotics Ltd nor the names of its contributors 
# may be used to endorse or promote products derived from this software without 
# specific prior written permission.

# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
# DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE 
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL 
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
# SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
# CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
# OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

import logging
import math
import time
import Queue
import mini_driver
import threading

#--------------------------------------------------------------------------------------------------- 
class RobotController:
    
    MIN_ANGLE = 0.0
    MAX_ANGLE = 180.0
    CENTRE_ANGLE = (MIN_ANGLE + MAX_ANGLE)/2.0
    
    MAX_UPDATE_TIME_DIFF = 0.25
    TIME_BETWEEN_SERVO_SETTING_UPDATES = 1.0
    TIME_BETWEEN_SENSOR_CONFIGURATION_UPDATES = 0.5
    
    JOYSTICK_DEAD_ZONE = 0.1
    MAX_ABS_NECK_SPEED = 30.0   # Degrees per second
    
    MOTION_COMMAND_TIMEOUT = 2.0 # If no commands for the motors are recieved in this time then
                                 # the motors (drive and servo) are set to zero speed
    
    #-----------------------------------------------------------------------------------------------
    def __init__( self, robotConfig ):
        
        self.miniDriver = mini_driver.MiniDriver()
        connected = self.miniDriver.connect()
        if not connected:
            raise Exception( "Unable to connect to the mini driver" )
        
        self.robotConfig = robotConfig
        self.leftMotorSpeed = 0
        self.rightMotorSpeed = 0
        self.panAngle = self.CENTRE_ANGLE
        self.tiltAngle = self.CENTRE_ANGLE
        
        self.panSpeed = 0.0
        self.tiltSpeed = 0.0
        
        self.lastServoSettingsSendTime = 0.0
        self.lastSensorConfigurationSendTime = 0.0
        self.lastUpdateTime = 0.0
        self.lastMotionCommandTime = time.time()
        
        self.piSensorModuleName = ""
        self.piSensorModule = None
        self.piSensorReader = None
        self.piSensorDict = {}
        
        self.Vmax_meter_per_second = 0.31
        self.Vstall_perc = 40.0
        self.ticks_per_meter = 97.9415
        self.ticks_per_degree = 0.1256
        self.Kp_l = 0.6
        self.Ki_l = 0.07
        self.Kd_l = 0.008
        self.Kp_r = 0.6
        self.Ki_r = 0.006
        self.Kd_r = 0.006
        self.Kp_b = 1.0
        self.Ki_b = 0.06
        self.Kd_b = 0.009
        self.interval = 0.04
        self.min_interval = 0.036
        self.max_interval = 0.044
        self.error_sum_l = 0.0
        self.error_sum_r = 0.0
        self.error_sum_b = 0.0
        self.error_prev_l = 0.0
        self.error_prev_r = 0.0
        self.error_prev_b = 0.0
        self.encoder_l -= 0
        self.encoder_r -= 0
        self.setpoint_l = 0.0
        self.setpoint_r = 0.0
        self.setpoint_b = 0.0
        self.ir_range = 0
        self.encoder_l = 0
        self.encoder_r = 0
        self.us_range = 0
        self.compass = 0.0
        self.Vmax_ticks_per_second = round((self.Vmax_meter_per_second * self.ticks_per_meter),0)    
        self.Vstall_ticks_per_second = round((self.Vstall_perc / 100 * self.Vmax_ticks_per_second),0)
        self.Vtarget_ticks_per_interval = round((self.Vmax_ticks_per_second * self.interval),0)
        self.Vperc_l = 0.0
        self.Vperc_r = 0.0
        self.centreNeck()
        self.setMotorSpeeds ( self.Vperc_l, self.Vperc_r)
    
    #-----------------------------------------------------------------------------------------------
    def __del__( self ):
        
        self.disconnect()
    
    #-----------------------------------------------------------------------------------------------
    def disconnect( self ):
        
        self.miniDriver.disconnect()
    
    #-----------------------------------------------------------------------------------------------
    def getStatusDict( self ):
        
        presetMaxAbsMotorSpeed, presetMaxAbsTurnSpeed = self.miniDriver.getPresetMotorSpeeds()
        
        statusDict = {
            "batteryVoltage" : self.miniDriver.getBatteryVoltageReading().data,
            "presetMaxAbsMotorSpeed" : presetMaxAbsMotorSpeed,
            "presetMaxAbsTurnSpeed" : presetMaxAbsTurnSpeed,
            "sensors" : self.getSensorDict()
        }
        
        return statusDict
        
    #-----------------------------------------------------------------------------------------------
    def getSensorDict( self ):
                
        sensorDict = {
            "batteryVoltage" : self.miniDriver.getBatteryVoltageReading(),
            "digital" : self.miniDriver.getDigitalReadings(),
            "analog" : self.miniDriver.getAnalogReadings(),
            "ultrasonic" : self.miniDriver.getUltrasonicReading(),
            "encoders" : self.miniDriver.getEncodersReading(),
        }
        
        sensorDict.update( self.piSensorDict )
        
        return sensorDict
    
    #-----------------------------------------------------------------------------------------------
    def normaliseJoystickData( self, joystickX, joystickY ):
        
        stickVectorLength = math.sqrt( joystickX**2 + joystickY**2 )
        if stickVectorLength > 1.0:
            joystickX /= stickVectorLength
            joystickY /= stickVectorLength
        
        if stickVectorLength < self.JOYSTICK_DEAD_ZONE:
            joystickX = 0.0
            joystickY = 0.0
            
        return ( joystickX, joystickY )
    
    #-----------------------------------------------------------------------------------------------
    def centreNeck( self ):
        
        self.panAngle = self.CENTRE_ANGLE
        self.tiltAngle = self.CENTRE_ANGLE
        self.panSpeed = 0.0
        self.tiltSpeed = 0.0
    
    #-----------------------------------------------------------------------------------------------
    def setMotorJoystickPos( self, joystickX, joystickY ):
        
        joystickX, joystickY = self.normaliseJoystickData( joystickX, joystickY )
        
        if self.robotConfig.usePresetMotorSpeeds:
            
            maxAbsMotorSpeed, maxAbsTurnSpeed = self.miniDriver.getPresetMotorSpeeds()
            
        else:
            
            maxAbsMotorSpeed = self.robotConfig.customMaxAbsMotorSpeed
            maxAbsTurnSpeed = self.robotConfig.customMaxAbsTurnSpeed
        
        # Set forward speed from joystickY
        leftMotorSpeed = maxAbsMotorSpeed*joystickY
        rightMotorSpeed = maxAbsMotorSpeed*joystickY
        
        # Set turn speed from joystickX
        leftMotorSpeed += maxAbsTurnSpeed*joystickX
        rightMotorSpeed -= maxAbsTurnSpeed*joystickX
        
        leftMotorSpeed = max( -maxAbsMotorSpeed, min( leftMotorSpeed, maxAbsMotorSpeed ) )
        rightMotorSpeed = max( -maxAbsMotorSpeed, min( rightMotorSpeed, maxAbsMotorSpeed ) )
        
        self.leftMotorSpeed = leftMotorSpeed*self.robotConfig.leftMotorScale
        self.rightMotorSpeed = rightMotorSpeed
        
        self.lastMotionCommandTime = time.time()
    
    #-----------------------------------------------------------------------------------------------
    def setMotorSpeeds( self, leftMotorSpeed, rightMotorSpeed ):
        
        if self.robotConfig.usePresetMotorSpeeds:
            
            maxAbsMotorSpeed, maxAbsTurnSpeed = self.miniDriver.getPresetMotorSpeeds()
            
        else:
            
            maxAbsMotorSpeed = self.robotConfig.customMaxAbsMotorSpeed
            maxAbsTurnSpeed = self.robotConfig.customMaxAbsTurnSpeed
        
        self.leftMotorSpeed = max( -maxAbsMotorSpeed, min( leftMotorSpeed, maxAbsMotorSpeed ) )
        self.rightMotorSpeed = max( -maxAbsMotorSpeed, min( rightMotorSpeed, maxAbsMotorSpeed ) )
        
        self.lastMotionCommandTime = time.time()
    
    #-----------------------------------------------------------------------------------------------
    def setNeckJoystickPos( self, joystickX, joystickY ):
        
        joystickX, joystickY = self.normaliseJoystickData( joystickX, joystickY )
        
        # Set pan and tilt angle speeds
        self.panSpeed = -self.MAX_ABS_NECK_SPEED*joystickX
        self.tiltSpeed = -self.MAX_ABS_NECK_SPEED*joystickY
        
        self.lastMotionCommandTime = time.time()
    
    #-----------------------------------------------------------------------------------------------
    def setNeckAngles( self, panAngle, tiltAngle ):
        
        self.panAngle = max( self.MIN_ANGLE, min( panAngle, self.MAX_ANGLE ) )
        self.tiltAngle = max( self.MIN_ANGLE, min( tiltAngle, self.MAX_ANGLE ) )
        self.panSpeed = 0.0
        self.tiltSpeed = 0.0
        
        self.lastMotionCommandTime = time.time()
    
    #-----------------------------------------------------------------------------------------------
    def _loadPiSensorModule( self ):
        
        if self.robotConfig.piSensorModuleName != "":
            
            # Try to import the new sensor module
            newSensorModule = None
            try:
                
                newSensorModule = __import__( self.robotConfig.piSensorModuleName, fromlist=[''] )
                
            except Exception as e:
                logging.error( "Caught exception when trying to import Pi sensor module" )
                logging.error( str( e ) )
                
            if newSensorModule != None:
                
                # We have a new sensor module. Shutdown any existing sensor reader
                if self.piSensorReader != None:
                    self.piSensorReader.shutdown()
                    self.piSensorReader = None
                    
                # Remove reference to existing sensor module
                self.piSensorModule = None
                self.piSensorModuleName = ""
                
                # Try to create the new Pi sensor reader
                newSensorReader = None
                
                try:
                    
                    newSensorReader = newSensorModule.PiSensorReader()
                
                except Exception as e:
                    logging.error( "Caught exception when trying to create Pi sensor reader" )
                    logging.error( str( e ) )
                    
                if newSensorReader != None:
                    self.piSensorModule = newSensorModule
                    self.piSensorModuleName = self.robotConfig.piSensorModuleName
                    self.piSensorReader = newSensorReader
    
    #-----------------------------------------------------------------------------------------------
    def update( self ):
        
        if not self.miniDriver.isConnected():
            return
        
        curTime = time.time()
        timeDiff = min( curTime - self.lastUpdateTime, self.MAX_UPDATE_TIME_DIFF )
        
        # Turn off the motors if we haven't received a motion command for a while
        if curTime - self.lastMotionCommandTime > self.MOTION_COMMAND_TIMEOUT:

            self.leftMotorSpeed = 0.0
            self.rightMotorSpeed = 0.0
            self.panSpeed = 0.0
            self.tiltSpeed = 0.0
        
        # Update the pan and tilt angles
        self.panAngle += self.panSpeed*timeDiff
        self.tiltAngle += self.tiltSpeed*timeDiff
        
        self.panAngle = max( self.MIN_ANGLE, min( self.panAngle, self.MAX_ANGLE ) )
        self.tiltAngle = max( self.MIN_ANGLE, min( self.tiltAngle, self.MAX_ANGLE ) )
        
        # Update the mini driver
        self.miniDriver.setOutputs(
            self.leftMotorSpeed, self.rightMotorSpeed, self.panAngle, self.tiltAngle )
        self.miniDriver.update()
        
        # Send servo settings if needed
        if curTime - self.lastServoSettingsSendTime >= self.TIME_BETWEEN_SERVO_SETTING_UPDATES:
            
            self.miniDriver.setPanServoLimits( 
                self.robotConfig.panPulseWidthMin, 
                self.robotConfig.panPulseWidthMax )
            self.miniDriver.setTiltServoLimits( 
                self.robotConfig.tiltPulseWidthMin, 
                self.robotConfig.tiltPulseWidthMax )
 
            self.lastServoSettingsSendTime = curTime
        
        # Send sensor configuration if needed
        if curTime - self.lastSensorConfigurationSendTime >= self.TIME_BETWEEN_SENSOR_CONFIGURATION_UPDATES:
            
            self.miniDriver.setSensorConfiguration( self.robotConfig.miniDriverSensorConfiguration )
 
            self.lastSensorConfigurationSendTime = curTime
        
        # Change the Pi sensor module if needed
        if self.robotConfig.piSensorModuleName != self.piSensorModuleName:
            self._loadPiSensorModule()
        
        # Read from any sensors attached to the Pi
        if self.piSensorReader != None:
            
            self.piSensorDict = {}
            try:
                self.piSensorDict = self.piSensorReader.readSensors()
            except Exception as e:
                logging.error( "Caught exception when trying to read from Pi sensor reader" )
                logging.error( str( e ) )
        
        self.lastUpdateTime = curTime
        
    ################################################################################################
    # The following routines are added for PID controlled driving                                  #
    ################################################################################################
    #-----------------------------------------------------------------------------------------------
    def set_new_gain_values (self, Kp_l, Ki_l, Kd_l, Kp_r, Ki_r, Kd_r, Kp_b, Ki_b, Kd_b ):
        self.Kp_l = Kp_l
        self.Ki_l = Ki_l
        self.Kd_l = Kd_l
        self.Kp_r = Kp_r
        self.Ki_r = Ki_r
        self.Kd_r = Kd_r
        self.Kp_b = Kp_b
        self.Ki_b = Ki_b
        self.Kd_b = Kd_b
    #-----------------------------------------------------------------------------------------------
    def time_out (self, milsec):
        for i in xrange (milsec):
            time.sleep (0.001)
    #-----------------------------------------------------------------------------------------------
    def get_sensor_readings (self):
        self.update()
        self.status_dict, _ = self.getStatusDict()
        self.sensor_dict = self.status_dict[ "sensors" ]
        self.ir_range = self.sensor_dict[ "digital" ][ "data" ]
        self.encoder_data = self.sensor_dict[ "encoders" ][ "data" ]
        self.encoder_l = self.encoder_data [0]
        self.encoder_r = self.encoder_data [1]
        self.us_range = self.sensor_dict[ "ultrasonic" ][ "data" ]
        self.compass = self.sensor_dict[ "compass" ][ "data" ]
    #-----------------------------------------------------------------------------------------------
    def reset_encoder_ticks (self):
        self.encoder_start_l = self.encoder_l
        self.encoder_start_r = self.encoder_r
    #-----------------------------------------------------------------------------------------------
    def get_actual_encoder_ticks(self):
        self.encoder_l -= self.encoder_start_l
        self.encoder_r -= self.encoder_start_r
    #-----------------------------------------------------------------------------------------------
    def update_setpoints(self):
        self.setpoint_l = round((self.encoder_l + self.Vtarget_ticks_per_interval),0)
        self.setpoint_r = round((self.encoder_r + self.Vtarget_ticks_per_interval),0)
    #-----------------------------------------------------------------------------------------------
    def control_loop_motors (self):
        self.error_l = self.setpoint_l - self.encoder_l
        self.error_sum_l = self.error_sum_l + self.error_l 
        self.P = self.Kp_l * self.error_l
        self.I = self.Ki_l * self.error_sum_l * self.interval
        self.D = self.Kd_l * (self.error_l - self.error_prev_l) / self.interval
        self.PID_l =  self.P + self.I + self.D
        self.error_prev_l = self.error_l
        self.Vticks_per_second_l = self.Vticks_per_second_l + self.PID_l
    
        self.error_r = self.setpoint_r - self.encoder_r
        self.error_sum_r = self.error_sum_r + self.error_r 
        self.P = self.Kp_r * self.error_r
        self.I = self.Ki_r * self.error_sum_r * self.interval
        self.D = self.Kd_r * (self.error_r - self.error_prev_r) / self.interval
        self.PID_r =  self.P + self.I + self.D
        self.error_prev_r = self.error_r
        self.Vticks_per_second_r = self.Vticks_per_second_r + self.PID_r
    #-----------------------------------------------------------------------------------------------
    def control_loop_balancing (self):
        self.error_b = self.setpoint_b - round ((self.compass),2)
        self.error_sum_b = self.error_sum_b + self.error_b
        self.P = self.Kp_b * self.error_b
        self.I = self.Ki_b * self.error_sum_b * self.interval
        self.D = self.Kd_b * (self.error_b - self.error_prev_b) / self.interval
        self.PID_b =  (self.P + self.I + self.D) * self.ticks_per_degree
        self.error_prev_b = self.error_b
        self.Vticks_per_second_l = round((self.Vticks_per_second_l - 0.5 * self.PID_b), 0)
        self.Vticks_per_second_r = round((self.Vticks_per_second_r + 0.5 * self.PID_b), 0)
        self.Vticks_per_second_l = max ( self.Vstall_ticks_per_second,
                                         min ( self.Vticks_per_second_l, 
                                               self.Vmax_ticks_per_second))
        self.Vticks_per_second_r = max ( self.Vstall_ticks_per_second,
                                         min ( self.Vticks_per_second_r, 
                                               self.Vmax_ticks_per_second))
    #-----------------------------------------------------------------------------------------------
    def drive_straight (meters_to_go): 
        self.errorValuesList = []
        self.get_sensor_readings()
        self.reset_encoder_ticks()
        self.get_actual_encoder_ticks()
        self.update_setpoints ()
        self_setpoint_b = round((self.compass), 2)
        self.Vticks_per_second_l = self.Vstall_ticks_per_second
        self.Vticks_per_second_r = self.Vstall_ticks_per_second
        self.Vperc_l = self.Vstall_perc
        self.Vperc_r = self.Vstall_perc
        self.setMotorSpeeds ( self.Vperc_l, self.Vperc_r)
        self.interval_start = time.clock()       
        self.meters_made = 0
        while self.meters_made <= meters_to_go:
            self.interval = round((time.clock () - self.interval_start), 2)
            if self.interval > self.min_interval:
                self.interval_start = time.clock()                                    # Reset interval timer
                if self.interval <= self.max_interval:
                    self.control_loop_motors()
                    self.control_loop_balancing()
                    self.Vperc_l = round((self.Vticks_per_second_l / self.Vmax_ticks_per_second * 100),1)
                    self.Vperc_l = max ( self.Vstall_perc, min ( self.Vperc_l, 100 ) )
                    self.Vperc_r = round((self.Vticks_per_second_r / self.Vmax_ticks_per_second * 100),1)
                    self.Vperc_r = max ( self.Vstall_perc, min ( self.Vperc_r, 100 ) )
                self.keep_error_values()
                self.update_setpoints ()
            self.setMotorSpeeds ( self.Vperc_l, self.Vperc_r)
            self.get_sensor_readings ()
            self.get_actual_encoder_ticks()
            self.ticks_made = max (self.encoder_l, self.encoder_r)
            self.meters_made = self.ticks_made / self.ticks_per_meter
        self.Vperc_l = 0.0
        self.Vperc_r = 0.0
        self.setMotorSpeeds ( self.Vperc_l, self.Vperc_r)
        time_out(100)
    #-----------------------------------------------------------------------------------------------
    def turn_degrees (self, direction, degrees):
        if direction == 'left':
            self.Vperc_l = -84.0
            self.Vperc_r = 84.0
        else:
            self.Vperc_l = 100.0
            self.Vperc_r = -100.0
        self.get_sensor_readings()
        self.reset_encoder_ticks()
        self.get_actual_encoder_ticks()
        self.ticks_turned = max (self.encoder_l, self.encoder_r)
        self.target_ticks = round((degrees * self.ticks_per_degree),0)
        while self.ticks_turned < self.target_ticks:
            self.setMotorSpeeds ( self.Vperc_l, self.Vperc_r)
            self.get_sensor_readings()
            self.get_actual_encoder_ticks()
            self.ticks_turned = max (self.encoder_l, self.encoder_r)
        self.Vperc_l = 0.0
        self.Vperc_r = 0.0
        self.setMotorSpeeds ( self.Vperc_l, self.Vperc_r)
        time_out(100)
    #-----------------------------------------------------------------------------------------------
    def keep_error_values(self):
        errorValues = {}
        errorValues[ "Tsample" ] = self.interval
        errorValues[ "Vperc_l" ] = self.Vperc_l
        errorValues[ "Vticks_l"] = self.Vticks_per_second_l
        errorValues[ "setpnt_l" ] = self.setpoint_l
        errorValues[ "encdr_l" ] = self.encoder_l
        errorValues[ "error_l" ] = self.error_l
        errorValues[ "PID_l" ] = self.PID_l
        errorValues[ "Vperc_r" ] = self.Vperc_r
        errorValues[ "Vticks_r"] = self.Vticks_per_second_r
        errorValues[ "setpnt_r" ] = self.setpoint_r
        errorValues[ "encdr_r" ] = self.encoder_r
        errorValues[ "error_r" ] = self.error_r
        errorValues[ "PID_r" ] = self.PID_r
        errorValues[ "compass" ] = self.compass
        errorValues[ "error_b" ] = self.error_b
        errorValues[ "PID_b" ] = self.PID_b
        self.errorValuesList.append( errorValues )
    #-----------------------------------------------------------------------------------------------
    def write_error_values():
        self.outputFilename = "Odm_1_values_{0}.csv".format( int( time.time() ) )
        with open( self.outputFilename, "w" ) as csvFile:
            dictWriter = csv.DictWriter( csvFile, 
                [ "Tsample",
                "Vperc_l", "Vticks_l", "setpnt_l", "encdr_l", "error_l", "PID_l", 
                "Vperc_r", "Vticks_r", "setpnt_r", "encdr_r", "error_r", "PID_r",
                "compass", "error_b", "PID_b"] )
            dictWriter.writeheader()
            dictWriter.writerows( self.errorValuesList )
