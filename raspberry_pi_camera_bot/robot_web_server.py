#! /usr/bin/env python

import logging

LOG_FILENAME = "/tmp/robot_web_server_log.txt"
logging.basicConfig( filename=LOG_FILENAME, level=logging.DEBUG )

# Also log to stdout
consoleHandler = logging.StreamHandler()
consoleHandler.setLevel( logging.DEBUG )
logging.getLogger( "" ).addHandler( consoleHandler )

import os.path
import math
import time
import signal
import tornado.httpserver
import tornado.ioloop
import tornado.web
import tornado.escape
import sockjs.tornado
import threading
import Queue
import camera_streamer
import robot_controller
import robot_config
import json
import ino_uploader
import subprocess

robot = None
robotConfig = robot_config.RobotConfig()

cameraStreamer = None
scriptPath = os.path.dirname( __file__ )
webPath = os.path.abspath( scriptPath + "/www" )
robotConnectionResultQueue = Queue.Queue()
isClosing = False

#--------------------------------------------------------------------------------------------------- 
def createRobot( robotConfig, resultQueue ):
    
    r = robot_controller.RobotController( robotConfig )
    resultQueue.put( r )
            
#--------------------------------------------------------------------------------------------------- 
class ConnectionHandler( sockjs.tornado.SockJSConnection ):
    
    #-----------------------------------------------------------------------------------------------
    def on_open( self, info ):
        
        pass
        
    #-----------------------------------------------------------------------------------------------
    def on_message( self, message ):
                
        try:
            message = str( message )
        except Exception:
            logging.warning( "Got a message that couldn't be converted to a string" )
            return

        if isinstance( message, str ):
            
            lineData = message.split( " " )
            if len( lineData ) > 0:
                
                if lineData[ 0 ] == "Centre":
                
                    if robot != None:
                        robot.centreNeck()
                
                elif lineData[ 0 ] == "StartStreaming":
                    
                    cameraStreamer.startStreaming()
                    
                elif lineData[ 0 ] == "Shutdown":
                    
                     result = subprocess.call( [ "poweroff" ] )
                     logging.info( "Shutdown request made. Result of call to poweroff was " + str( result ) )
                    
                elif lineData[ 0 ] == "GetConfig":
                    
                    # Get the current configuration and return it
                    configDict = robotConfig.getConfigDict()
                    
                    self.send( json.dumps( configDict, default=lambda o: o.__dict__, separators=(',',':') ) )
                
                elif lineData[ 0 ] == "SetConfig" and len( lineData ) >= 2:
                    
                    # Send the new configuration to the robot
                    configDict = {}
                    
                    print "Got config data", lineData[ 1 ]
                    
                    try:
                        configDict = json.loads( lineData[ 1 ] )
                    except Exception:
                        pass
                    
                    robotConfig.setConfigDict( configDict )
                
                elif lineData[ 0 ] == "GetRobotStatus":
                    
                    # Get the current status from the robot and return it
                    statusDict = {}
                    
                    if robot != None:
                        statusDict = robot.getStatusDict()
                    
                    self.send( json.dumps( statusDict, default=lambda o: o.__dict__, separators=(',',':') ) )
                
                elif lineData[ 0 ] == "GetServerTime":
                    
                    self.send( json.dumps( { "serverTime" : time.time() }, default=lambda o: o.__dict__, separators=(',',':') ) )
                
                elif lineData[ 0 ] == "GetLogs":
                    
                    # Return a dictionary containing the current logs
                    self.send( json.dumps( self.getLogsDict(), default=lambda o: o.__dict__, separators=(',',':') ) )
                
                elif lineData[ 0 ] == "Move" and len( lineData ) >= 3:
                    
                    motorJoystickX, motorJoystickY = \
                        self.extractJoystickData( lineData[ 1 ], lineData[ 2 ] )
                    
                    if robot != None:
                        robot.setMotorJoystickPos( motorJoystickX, motorJoystickY )     
                        
                elif lineData[ 0 ] == "SetMotorSpeeds" and len( lineData ) >= 3:
                    
                    leftMotorSpeed = 0.0
                    rightMotorSpeed = 0.0
                    
                    try:
                        leftMotorSpeed = float( lineData[ 1 ] )
                    except Exception:
                        pass
                    
                    try:
                        rightMotorSpeed = float( lineData[ 2 ] )
                    except Exception:
                        pass
                    
                    if robot != None:
                        robot.setMotorSpeeds( leftMotorSpeed, rightMotorSpeed )
                    
                elif lineData[ 0 ] == "PanTilt" and len( lineData ) >= 3:
                    
                    neckJoystickX, neckJoystickY = \
                        self.extractJoystickData( lineData[ 1 ], lineData[ 2 ] )
                        
                    if robot != None:
                        robot.setNeckJoystickPos( neckJoystickX, neckJoystickY )
                        
                elif lineData[ 0 ] == "SetNeckAngles" and len( lineData ) >= 3:
                    
                    panAngle = robot_controller.RobotController.CENTRE_ANGLE
                    tiltAngle = robot_controller.RobotController.CENTRE_ANGLE
                    
                    try:
                        panAngle = float( lineData[ 1 ] )
                    except Exception:
                        pass
                    
                    try:
                        tiltAngle = float( lineData[ 2 ] )
                    except Exception:
                        pass
                    
                    if robot != None:
                        robot.setNeckAngles( panAngle, tiltAngle )
                
                elif lineData[ 0 ] == "SetNewGainValues" and len( lineData ) >= 9:
                    
                    Kp_l = 0.4
                    Ki_l = 0.02
                    Kd_l = 0.0
                    Kp_r = 0.4
                    Ki_r = 0.02
                    Kd_r = 0.0
                    Kp_b = 0.4
                    Ki_b = 0.02
                    Kd_b = 0.0
                    
                    try:
                        Kp_l = float( lineData[ 1 ] )
                    except Exception:
                        pass
                    try:
                        Ki_l = float( lineData[ 2 ] )
                    except Exception:
                        pass
                    try:
                        Kd_l = float( lineData[ 3 ] )
                    except Exception:
                        pass
                    try:
                        Kp_r = float( lineData[ 4 ] )
                    except Exception:
                        pass
                    try:
                        Ki_r = float( lineData[ 5 ] )
                    except Exception:
                        pass
                    try:
                        Kd_r = float( lineData[ 6 ] )
                    except Exception:
                        pass
                    try:
                        Kp_b = float( lineData[ 7 ] )
                    except Exception:
                        pass
                    try:
                        Ki_b = float( lineData[ 8 ] )
                    except Exception:
                        pass
                    try:
                        Kd_b = float( lineData[ 9 ] )
                    except Exception:
                        pass
                    
                    if robot != None:
                        robot.set_new_gain_values (Kp_l,Ki_l,Kd_l,Kp_r,Ki_r,Kd_r,Kp_b,Ki_b,Kd_b )
                        
                elif lineData[ 0 ] == "DriveStraight" and len( lineData ) >= 2:
                    
                    meters_to_go = 0.0
                    
                    try:
                        meters_to_go = float( lineData[ 1 ] )
                    except Exception:
                        pass
                    
                    if robot != None:
                        robot.drive_straight (meters_to_go)
                    
                elif lineData[ 0 ] == "TurnDegrees" and len( lineData ) >= 3:
                    
                    direction = ""
                    degrees = 0
                    
                    try:
                        degrees = integer( lineData[ 2 ] )
                    except Exception:
                        pass
                    
                    direction = lineData[ 1 ]
                    
                    if robot != None:
                        robot.turn_degrees (direction, degrees)
                
                elif lineData[ 0 ] == "DumpToFile" 
                                        
                    if robot != None:
                        robot.write_error_values()
                    
    #-----------------------------------------------------------------------------------------------
    def on_close( self ):
        logging.info( "SockJS connection closed" )

    #-----------------------------------------------------------------------------------------------
    def getLogsDict( self ):
        
        logsDict = {}
        
        # Read in main logs file
        try:
            with open( LOG_FILENAME, "r" ) as logFile:
                logsDict[ "MainLog" ] = logFile.read()
        except Exception:
            pass
        
        # Read in Ino build output if it exists
        try:
            with open( ino_uploader.BUILD_OUTPUT_FILENAME, "r" ) as logFile:
                logsDict[ "InoBuildLog" ] = logFile.read()
        except Exception:
            pass

        return logsDict
        
    #-----------------------------------------------------------------------------------------------
    def extractJoystickData( self, dataX, dataY ):
        
        joystickX = 0.0
        joystickY = 0.0
        
        try:
            joystickX = float( dataX )
        except Exception:
            pass
        
        try:
            joystickY = float( dataY )
        except Exception:
            pass
            
        return ( joystickX, joystickY )

#--------------------------------------------------------------------------------------------------- 
class MainHandler( tornado.web.RequestHandler ):
    
    #------------------------------------------------------------------------------------------------
    def get( self ):
        self.render( webPath + "/index.html" )
        
#--------------------------------------------------------------------------------------------------- 
def robotUpdate():
    
    global robot
    global isClosing
    
    if isClosing:
        tornado.ioloop.IOLoop.instance().stop()
        return
        
    if robot == None:
        
        if not robotConnectionResultQueue.empty():
            
            robot = robotConnectionResultQueue.get()
        
    else:
                
        robot.update()

#--------------------------------------------------------------------------------------------------- 
def signalHandler( signum, frame ):
    
    if signum in [ signal.SIGINT, signal.SIGTERM ]:
        global isClosing
        isClosing = True
        
        
#--------------------------------------------------------------------------------------------------- 
if __name__ == "__main__":
    
    signal.signal( signal.SIGINT, signalHandler )
    signal.signal( signal.SIGTERM, signalHandler )
    
    # Create the configuration for the web server
    router = sockjs.tornado.SockJSRouter( 
        ConnectionHandler, '/robot_control' )
    application = tornado.web.Application( router.urls + [ 
        ( r"/", MainHandler ), 
        ( r"/(.*)", tornado.web.StaticFileHandler, { "path": webPath } ),
        ( r"/css/(.*)", tornado.web.StaticFileHandler, { "path": webPath + "/css" } ),
        ( r"/css/images/(.*)", tornado.web.StaticFileHandler, { "path": webPath + "/css/images" } ),
        ( r"/images/(.*)", tornado.web.StaticFileHandler, { "path": webPath + "/images" } ),
        ( r"/js/(.*)", tornado.web.StaticFileHandler, { "path": webPath + "/js" } ) ] )
    
    #( r"/(.*)", tornado.web.StaticFileHandler, {"path": scriptPath + "/www" } ) ] \
    
    # Create a camera streamer
    cameraStreamer = camera_streamer.CameraStreamer()
    
    # Start connecting to the robot asyncronously
    robotConnectionThread = threading.Thread( target=createRobot, 
        args=[ robotConfig, robotConnectionResultQueue ] )
    robotConnectionThread.start()

    # Now start the web server
    logging.info( "Starting web server..." )
    http_server = tornado.httpserver.HTTPServer( application )
    http_server.listen( 80 )
    
    robotPeriodicCallback = tornado.ioloop.PeriodicCallback( 
        robotUpdate, 100, io_loop=tornado.ioloop.IOLoop.instance() )
    robotPeriodicCallback.start()
    
    cameraStreamerPeriodicCallback = tornado.ioloop.PeriodicCallback( 
        cameraStreamer.update, 1000, io_loop=tornado.ioloop.IOLoop.instance() )
    cameraStreamerPeriodicCallback.start()
    
    tornado.ioloop.IOLoop.instance().start()
    
    # Shut down code
    robotConnectionThread.join()
    
    if robot != None:
        robot.disconnect()
    else:
        if not robotConnectionResultQueue.empty():
            robot = robotConnectionResultQueue.get()
            robot.disconnect()
            
    cameraStreamer.stopStreaming