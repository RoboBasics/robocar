**##### Robo BAS ics #####** 

![RB Logo.jpg](https://bitbucket.org/repo/9yGMyB/images/2166640969-RB%20Logo.jpg)

This repo contains Python scripts written for the Pi Camera Robot [Dawnrobotics]

Hardware: Raspberry Pi B+ & Dagu Arduino mini driver

Software: 'py websockets bot' & 'raspberry pi camera streamer' libraries of Dawn Robotics, openCV and numpy

It also contains a folder 'Handy Stuff' with all soort of routines and snippets



RoboBASics versions:

RB-1
![RB1-small.jpg](https://bitbucket.org/repo/9yGMyB/images/2353461987-RB1-small.jpg)

RB-2
![RB2-b small.jpg](https://bitbucket.org/repo/9yGMyB/images/578949033-RB2-b%20small.jpg)

Video of reading_signs at: https://youtu.be/6eawQi86RUM

More info at:

http://www.instructables.com/id/Object-tracking-by-color-with-Python-and-openCV/

and 

http://www.instructables.com/id/RB2-running-squares/

and

http://www.instructables.com/id/Configure-read-data-calibrate-the-HMC5883L-digital/