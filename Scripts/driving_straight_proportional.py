#! /usr/bin/python

#-----------------------------------------------------------------------------------------------------------------------------------------------------------
# Most simple way to drive in a straight line by using encoder data:
# init encoders, read sensors, calculate proportional differance, apply offset to the motor speeds 
# Works okay when readings can be obtained at high rate; activating the display routine will bring performance down by > 60%
# As result of that,  the bot might run in a swirling mode. Furthermore: avoid print statements inside the loop 
#-----------------------------------------------------------------------------------------------------------------------------------------------------------
import time
import argparse
import cv2
import numpy as np
import math 
import py_websockets_bot
import py_websockets_bot.mini_driver
import py_websockets_bot.robot_config
import random
import csv
# -------------------------------------------------------------------------------------
# Set and initialize variables
# -------------------------------------------------------------------------------------
range_save = 40                                                                       # Safety limit 
motor_speed = 50.0                                                                    # = 50%; corr. to PMW 50Hz
encoder_data_start_l = 0.0
encoder_data_start_r = 0.0
pan_angle = 90
tilt_angle = 90
motor_l = 50.0
motor_r = 50.0
list_encoder = []
line_cnt = 25
font = cv2.FONT_HERSHEY_SIMPLEX
latest_camera_image = None
# -------------------------------------------------------------------------------------
# Optional routine display's image with data (consumes > 60% of performance!)
# -------------------------------------------------------------------------------------
def display():
    image, _  = bot.get_latest_camera_image()
    encoders = 'encoders   %d : %d ' % (encoder_left, encoder_right)
    distance = 'range      %d ' % (usrange_data)
    infrared = 'IR sensors %d ' % (infrared_data)
    speeds = 'motors     %d : %d ' % (motor_l, motor_r)
    cv2.putText(image, infrared ,(20, 25), font, 0.5,(0,255,0),2)
    cv2.putText(image, distance,(20, 45), font, 0.5,(0,255,0),2)    
    cv2.putText(image, encoders,(20, 65), font, 0.5,(0,0,255),2)
    cv2.putText(image, speeds,(20, 85), font, 0.5,(0,0,255),2)
    cv2.imshow("running",image)
    cv2.waitKey(1)
#-------------------------------------------------------------------------------------- Timer routine for accuracy
def time_out (milsec):
    for i in xrange (milsec):
        time.sleep (0.001)
# -------------------------------------------------------------------------------------
# set encoder counters to zero
# -------------------------------------------------------------------------------------
def encoder_init ():
    global encoder_data_start_l, encoder_data_start_r
    encoder_data_start_l = encoder_data [0]
    encoder_data_start_r = encoder_data [1]
    print 'encoders initialized'
# -------------------------------------------------------------------------------------
def read_sensors ():
    global infrared_data, encoder_data, encoder_left, encoder_right, encoder_offset, usrange_data 
    status_dict, _ = bot.get_robot_status_dict()
    sensor_dict = status_dict[ "sensors" ]
    infrared_data = sensor_dict[ "digital" ][ "data" ]
    encoder_data = sensor_dict[ "encoders" ][ "data" ]
    encoder_left = encoder_data [0] - encoder_data_start_l
    encoder_right = encoder_data [1]- encoder_data_start_r
    encoder_offset = encoder_left - encoder_right
    usrange_data = sensor_dict[ "ultrasonic" ][ "data" ]
    #list_encoder.append ([encoder_data, encoder_left, encoder_right,encoder_offset]) # For debugging. Is much faster than print statemens 
# -------------------------------------------------------------------------------------
def back_out ():
    bot.set_motor_speeds (0.0, 0.0)
    time_out(20)
    bot.set_motor_speeds(-50.0, -50.0)
    time_out(1000)
    bot.set_motor_speeds (0.0, 0.0)
    time_out(10)
    bot.set_motor_speeds( 50.0, -50.0 )
    time_out (900)
    bot.set_motor_speeds( 0.0, 0.0 )
    time_out (200)
    encoder_init()
    read_sensors()
# -------------------------------------------------------------------------------------
def find_free_range ():
    bot.set_motor_speeds( 0.0, 0.0 )
    time_out (10)
    pan_angle = 90
    range_l = 0
    range_r = 0 
    while pan_angle > 0:
        pan_angle -= 2
        bot.set_neck_angles(pan_angle, tilt_angle)
        time_out(50)
    read_sensors()
    if usrange_data > range_save and usrange_data < 400:
        bot.set_motor_speeds( 50.0, -50.0 )
        time_out (900)
        bot.set_motor_speeds( 0.0, 0.0 )
        time_out (10)
        bot.centre_neck()
        time_out (10)
    else:
        while pan_angle < 180:
            pan_angle += 2
            bot.set_neck_angles(pan_angle, tilt_angle)
            time_out (50)
        read_sensors()
        if usrange_data > range_save and usrange_data < 400:
            bot.set_motor_speeds( -50.0, 50.0 )
            time_out (900)
            bot.set_motor_speeds( 0.0, 0.0 )
            time_out (10)
            bot.centre_neck()
            time_out (10)
        else:
            bot.centre_neck()
            time_out (10)
            back_out()
    encoder_init()
    read_sensors()
#--------------------------------------------------------------------------------------- Set up a parser for command line arguments
parser = argparse.ArgumentParser( "Gets images from the robot using callbacks and displays them" )
parser.add_argument( "hostname", default="localhost", nargs='?',
                     help="The ip address of the robot" )
args = parser.parse_args()
# -------------------------------------------------------------------------------------- Connect to the robot
bot = py_websockets_bot.WebsocketsBot( "192.168.42.1" )
#--------------------------------------------------------------------------------------- Initialize mini driver pins       
sensorConfiguration = py_websockets_bot.mini_driver.SensorConfiguration(
    configD12=py_websockets_bot.mini_driver.PIN_FUNC_ULTRASONIC_READ,                  # SeeeD 3-pin Ultrasonic sensor
    configD13=py_websockets_bot.mini_driver.PIN_FUNC_INACTIVE, 
    configA0=py_websockets_bot.mini_driver.PIN_FUNC_ANALOG_READ, 
    configA1=py_websockets_bot.mini_driver.PIN_FUNC_DIGITAL_READ,                      # Sharp IR switch RIGHT
    configA2=py_websockets_bot.mini_driver.PIN_FUNC_DIGITAL_READ,                      # Sharp IR switch LEFT
    configA3=py_websockets_bot.mini_driver.PIN_FUNC_DIGITAL_READ,                      # Grove Line sensor RIGHT
    configA4=py_websockets_bot.mini_driver.PIN_FUNC_DIGITAL_READ,                      # Grove Line sensor MIDDLE
    configA5=py_websockets_bot.mini_driver.PIN_FUNC_DIGITAL_READ,                      # Grove Line sensor LEFT
    leftEncoderType=py_websockets_bot.mini_driver.ENCODER_TYPE_SINGLE_OUTPUT,          # Single encoder
    rightEncoderType=py_websockets_bot.mini_driver.ENCODER_TYPE_SINGLE_OUTPUT )        # Single encoder
robot_config = bot.get_robot_config()
robot_config.miniDriverSensorConfiguration = sensorConfiguration
bot.set_robot_config( robot_config )
bot.update()                                                                           # Update any background communications with the robot
time_out (100)                                                                         # Sleep to avoid overload of the web server on the robot
#--------------------------------------------------------------------------------------
if __name__ == "__main__":

    #bot.start_streaming_camera_images()
    #time_out(200)
    #bot.update()
    bot.centre_neck()
    read_sensors()
    encoder_init()
    while encoder_offset != 0:
        read_sensors ()
        time_out (1)
    while True:
        if infrared_data != 24:                                                         # Test for any IR sensor signal and maximum range
            back_out()
            while encoder_offset != 0:                                                  # Synchronise readings with actual state
                encoder_init ()
                read_sensors ()
                time_out (1)
        else:
            if usrange_data <= range_save:
                find_free_range ()
                while encoder_offset != 0:
                    encoder_init ()
                    read_sensors ()
                    time_out (1)
            else:
                speed_adjust = (abs(encoder_offset)* motor_speed) / 8.75                # most simple approach that worked fine  
                speed_adjust = round (speed_adjust, 1)
                if encoder_offset < 0:
                    motor_l = motor_speed + speed_adjust                                # to keep average speed constant 
                    motor_r = motor_speed - speed_adjust
                else:
                    if encoder_offset > 0:
                        motor_l = motor_speed - speed_adjust
                        motor_r = motor_speed + speed_adjust
                bot.set_motor_speeds(motor_l, motor_r)
                time_out(10)
        #bot.update()
        #display()
        read_sensors()
# -------------------------------------------------------------------------------------
    cv2.destroyAllWindows()
    bot.stop_streaming_camera_images()
    bot.set_motor_speeds( 0.0, 0.0 )
    bot.centre_neck()
    bot.disconnect()
    print 'FINISHED'
