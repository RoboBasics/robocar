import RPi.GPIO as GPIO
import smbus
import time
import math
import sensor_reading

class PiSensorReader:
  

    #-----------------------------------------------------------------------------------------------
    def __init__( self ):
    
        self.bus = smbus.SMBus(1)
        self.address = 0x1e
        self.declination = 0.42
        self.scale = 0.92
        self.x_offset = 25
        self.y_offset = -4
        
        self.x_out = 0.0
        self.y_out = 0.0
        self.z_out = 0.0
        self.bearing = 0.0
        
        self.write_byte( 0, 0b01110000 ) # Set to 8 samples at 15Hz
        self.write_byte( 1, 0b00100000 ) # 1.3 gain LSb / Gauss 1090 (default)
        self.write_byte( 2, 0b00000000 ) # Continuous sampling
    #-----------------------------------------------------------------------------------------------
    def shutdown( self ):

        pass
    #-----------------------------------------------------------------------------------------------
    def read_pi_sensors( self ):

        sensorDict = {}
        self.x_out = (self.read_word_2c(3) - self.x_offset) * self.scale
        self.y_out = (self.read_word_2c(7) - self.y_offset) * self.scale
        self.z_out = (self.read_word_2c(5)) * self.scale

        self.bearing  = math.atan2(self.y_out, self.x_out) 
        if (self.bearing < 0):
            self.bearing += 2 * math.pi
    
        self.sensorValue = math.degrees(self.bearing) + self.declination
        
        sensorReading = sensor_reading.SensorReading( sensorValue, timestamp=time.time() )
        sensorDict[ "compass" ] = sensorReading

        return sensorDict

    def read_byte(self, adr):
        return self.bus.read_byte_data(self.address, adr)
    
    def read_word(self, adr):
        high = self.bus.read_byte_data(self.address, adr)
        low = self.bus.read_byte_data(self.address, adr+1)
        val = (high << 8) + low
        return val
    
    def read_word_2c(self, adr):
        val = self.read_word(adr)
        if (val >= 0x8000):
            return -((65535 - val) + 1)
        else:
            return val
    
    def write_byte(self, adr, value):
        self.bus.write_byte_data(self.address, adr, value)
